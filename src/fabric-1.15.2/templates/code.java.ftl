<#-- @formatter:off -->
/**
 * This mod element is always locked. Enter your code in the methods below.
 *
 * You can register new events in this class too.
 *
 * If you want to make a plain independent class, create it using
 * Project Browser - New... and make sure to make the class
 * outside ${package} as this package is managed by MCreator.
 *
 * If you change workspace package, modid or prefix, you will need
 * to manually adapt this file to these changes or remake it.
*/

package ${package};

public class ${name} {
}
<#-- @formatter:on -->
